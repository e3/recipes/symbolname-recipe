# symbolname conda recipe

Home: "https://github.com/paulscherrerinstitute/symbolname"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS symbolname module
